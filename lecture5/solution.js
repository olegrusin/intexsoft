var phoneBook = [];

function Add(name, phone) {
  var phoneList = phone.split(",").join(", ");

  var contact = phoneBook.filter(function(item) {
    return item.indexOf(name) == 0;
  })[0];

  return contact
    ? (phoneBook[phoneBook.indexOf(contact)] += ", " + phoneList)
    : phoneBook.push(name + ": " + phoneList);
}

function Remove(phone) {
  var contact = phoneBook.filter(function(item) {
    return item.indexOf(phone) > 0;
  })[0];

  if (contact) {
    var phoneList = contact
      .split(":")[1]
      .split(",")
      .filter(function(item) {
        return item.indexOf(phone) < 0;
      })
      .toString();

    if (phoneList) {
      phoneBook[phoneBook.indexOf(contact)] = contact.split(":")[0] + ":" + phoneList;
    } else {
      phoneBook = phoneBook.filter(function(item) {
        return item.indexOf(phone) < 0;
      });
    }

    return true;
  } else {
    return false;
  }
}

module.exports = {
  getWords: function(sentence) {
    return sentence
      .split(" ")
      .filter(function(item) {
        return item[0] == "#";
      })
      .map(function(item) {
        return item.substring(1);
      });
  },
  normalizeWords: function(words) {
    return words
      .join()
      .toLowerCase()
      .split(",")
      .filter(function(item, i, arr) {
        return arr.indexOf(item) == i;
      })
      .join(", ");
  },
  addressBook: function(command) {
    arg = command.split(" ");
    switch (arg[0]) {
      case "ADD":
        return Add(arg[1], arg[2]);
      case "REMOVE_PHONE":
        return Remove(arg[1]);
      case "SHOW":
        return phoneBook.sort();
      default:
        return "Command not found!";
    }
  }
};
