module.exports = Collection;

function Collection() {
  this.array = [];
}

Collection.prototype.append = function(value) {
  if (value instanceof Collection) {
    this.array = this.array.concat(value.array);
    return this.array;
  }
  return this.array.push(value);
};

Collection.prototype.at = function(index) {
  return this.array[index - 1] ? this.array[index - 1] : null;
};

Collection.prototype.values = function() {
  return this.array;
};

Collection.prototype.count = function() {
  return this.array.length;
};

Collection.prototype.removeAt = function(index) {
  if (this.array[index - 1]) {
    this.array.splice(index - 1, 1);
    return true;
  }
  return false;
};

Collection.from = function() {
  var collection = new Collection();
  collection.array = collection.array.concat([].slice.call(arguments).flat());
  return collection;
};
