var circleX = 3,
  circleY = 5,
  circleR = 4;

function isPointInCircle(x, y) {
  return (
    Math.pow(circleX - x, 2) + Math.pow(circleY - y, 2) <= Math.pow(circleR, 2)
  );
}

function isPointInQuadrangle(x, y) {
  return (
    y <= -0.6 * x + 3 &&
    y >= -1.5 * x - 12 &&
    y <= (4 / 7) * x + 4 &&
    y >= 0.4 * x - 2
  );
}
