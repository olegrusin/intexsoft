function addMinutes(hours, minutes, number) {
  var amountMinutes = hours * 60 + minutes + number;
  hours = Math.trunc(amountMinutes / 60);
  minutes = amountMinutes % 60;
  return hours >= 24
    ? addMinutes(hours - 24, minutes, 0)
    : (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
}
