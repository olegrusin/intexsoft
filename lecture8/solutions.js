function query(collection) {
  var newCollection = [].slice.call(collection);
  var args = [].slice.call(arguments, 1).sort();

  for (var i = 0; i < args.length; i++) {
    newCollection = args[i](newCollection);
  }

  return newCollection;
}

function select() {
  var fields = [].slice.call(arguments);
  
  return function(collection) {
    return collection.map(function(element) {
      return fields.reduce(function(accumulator, fieldName) {
        accumulator[fieldName] = element[fieldName];
        return accumulator;
      }, {});
    });
  };
}

function filterIn(property, values) {
  return function(collection) {
    return collection.filter(function(element) {
      return values.includes(element[property]);
    });
  };
}

module.exports = {
  timeShift: function(date) {
    var obj = {
      init: function(date) {
        this.time = new Date(date);
        return this;
      },

      unit: {
        years: "FullYear",
        months: "Month",
        days: "Date",
        hours: "Hours",
        minutes: "Minutes"
      },

      add: function(number, type) {
        return this.setValue(number, this.unit[type]);
      },

      subtract: function(number, type) {
        return this.setValue(-number, this.unit[type]);
      },

      setValue: function(value, method) {
        this.time["set" + method](this.time["get" + method]() + value);
        return this;
      },

      toString: function() {
        return this.time.toLocaleString("be", {
          day: "2-digit",
          year: "numeric",
          month: "2-digit",
          hour: "numeric",
          minute: "numeric"
        });
      }
    };

    Object.defineProperty(obj, "value", {
      get: function() {
        return this + "";
      }
    });

    return obj.init(date);
  },
  lib: {
    query: query,
    select: select,
    filterIn: filterIn
  }
};
