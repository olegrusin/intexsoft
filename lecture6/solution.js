var phoneBook = {};

function Add(name, phone) {
  var phoneList = phone.split(",");
  if (phoneBook.hasOwnProperty(name)) {
    phoneBook[name] = phoneBook[name].concat(phoneList);
  } else {
    phoneBook[name] = phoneList;
  }
}

function Remove(phone) {
  for (var prop in phoneBook) {
    if (phoneBook[prop].indexOf(phone) >= 0) {
      phoneBook[prop] = phoneBook[prop].filter(item => item !== phone);
      if (phoneBook[prop] == false) {
        delete phoneBook[prop];
      }
      return true;
    }
  }
  return false;
}

function Show() {
  var result = [];
  for (var prop in phoneBook) {
    result.push(prop + ": " + phoneBook[prop].join(", "));
  }
  return result.sort();
}

module.exports = {
  getWords: function(sentence) {},
  normalizeWords: function(words) {},
  addressBook: function(command) {
    arg = command.split(" ");
    switch (arg[0]) {
      case "ADD":
        return Add(arg[1], arg[2]);
      case "REMOVE_PHONE":
        return Remove(arg[1]);
      case "SHOW":
        return Show();
      default:
        return "Command not found!";
    }
  }
};
