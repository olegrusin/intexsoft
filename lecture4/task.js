/**
 * Returns the sum of numbers from 1 to n
 * @param {*} number a numeric expression
 */
function getSumm(number) {
  return number >= 0 ? number + getSumm(number - 1) : 0;
}

/**
 * Returns the factorial
 * @param {*} number a number expression
 */
function getFactorial(number) {
  return number ? number * getFactorial(number - 1)  : 1;
}
