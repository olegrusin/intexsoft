var content = document.getElementById('contentArea');
var input = document.createElement("input");
var send = document.createElement("a");
var clear = document.createElement("a");
var allFigure = document.createElement("div");
var area = document.createElement("div");
send.textContent = "send";
clear.textContent = "clear";

content.appendChild(input);
content.appendChild(send);
content.appendChild(clear);
content.appendChild(allFigure);
var figure = ['square', 'circle', 'roundsquare', 'squareround'];
for (var count in figure) {
    var newCheckbox = document.createElement('div');
    newCheckbox.className = 'figure';
    newCheckbox.id = figure[count];
    allFigure.appendChild(newCheckbox);
}

content.appendChild(area);

var selectFigure;
content.onclick = function (event) {
    var target = event.target;
    if (target.className != 'figure') return;
    if (selectFigure) {
        selectFigure.style.borderColor = "";
    }
    selectFigure = target;
    selectFigure.style.borderColor = "#ee1111";
}

function renderNumber(number) {
    for (var i = 0; i < number; i++) {
        for (var j = 0; j < number; j++) {
            var item = area.appendChild(selectFigure.cloneNode(true));
            item.style.display = "inline-block";
            item.style.backgroundColor = '#' + Math.random().toString(16).substring(2, 8);
            item.style.borderColor = item.style.backgroundColor;
        }
        area.innerHTML += '<br>';
    }
}

send.onclick = function () {
    return input.value < 31 && input.value > 0 ? renderNumber(input.value) : alert('Eneter correct number 1-30');
}

clear.onclick = function () {
    selectFigure.classList.remove('highlight');
    selectFigure.style.borderColor = "";
    selectFigure = null;
    return area.innerHTML = '';
}