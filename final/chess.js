var content = document.getElementById("contentArea");
var table = document.createElement("table");
var figure = document.createElement("div");

var selectFigure;
var selectCell;
var chesspiece = ["king", "queen", "rook", "bishop", "knight", "pawn"];

content.appendChild(figure);

for (var count in chesspiece) {
    var piece = document.createElement("p");
    piece.className = "fas fa-chess-" + chesspiece[count];
    figure.appendChild(piece).style.display = "inline-block";
}

for (var i = 0; i < 8; i++) {
    var row = document.createElement("tr");
    for (var j = 0; j < 8; j++) {
        var cell = document.createElement("td");
        row.appendChild(cell);
    }
    table.appendChild(row);
}

content.appendChild(table);

figure.addEventListener("click", function (event) {
    var target = event.target;
    if (target.tagName != "P") return;
    clearВoard();
    if (selectFigure) {
        selectFigure.style.color = "";
        selectCell.style.borderColor = "";
        selectCell.textContent = "";
    }
    selectFigure = target;
    selectFigure.style.color = "#ee1111";
});

table.addEventListener("click", function (event) {
    var target = event.target;
    if (target.tagName != "TD") return;
    clearВoard();
    if (selectCell) {
        selectCell.style.borderColor = "";
        selectCell.textContent = "";
    }
    selectCell = target;
    selectCell.style.backgroundColor = "#2d89ef";
    selectCell.innerHTML = '<p class="' + selectFigure.className + '"></p>';

    switch (selectFigure.classList[1].split("-")[2]) {
        case "king":
            return king(selectCell.parentNode.rowIndex, selectCell.cellIndex);
        case "queen":
            return queen(selectCell.parentNode.rowIndex, selectCell.cellIndex);
        case "rook":
            return rook(selectCell.parentNode.rowIndex, selectCell.cellIndex);
        case "bishop":
            return bishop(selectCell.parentNode.rowIndex, selectCell.cellIndex);
        case "knight":
            return knight(selectCell.parentNode.rowIndex, selectCell.cellIndex);
        case "pawn":
            return pawn(selectCell.parentNode.rowIndex, selectCell.cellIndex);
    }
});

function diagonal(x, y, item) {
    for (var i = 1; i <= item; i++) {
          nextStep(x + i, y - i);
          nextStep(x - i, y + i);
          nextStep(x - i, y - i);
          nextStep(x + i, y + i);
    }
  }
  function bodacious(x, y, item) {
    for (var i = 1; i <= item; i++) {
          nextStep(x, y + i);
          nextStep(x, y - i);
          nextStep(x + i, y);
          nextStep(x - i, y);
    }
  } 
  
  function king(x, y) {
      diagonal(x, y, 1);
      bodacious(x, y, 1);
  }
  
  function queen(x, y) {
      diagonal(x, y, 8);
      bodacious(x, y, 8);
  }
  
  function rook(x, y) {
      bodacious(x, y, 8);
  }
  
  function bishop(x, y) {
      diagonal(x, y, 8);
  }
  
  function knight(x, y) {
      nextStep(x + 2, y - 1);
      nextStep(x - 2, y + 1);
      nextStep(x - 2, y - 1);
      nextStep(x + 2, y + 1);
      nextStep(x + 1, y + 2);
      nextStep(x + 1, y - 2);
      nextStep(x - 1, y + 2);
      nextStep(x - 1, y - 2);
  }
  
  function pawn(x, y) {
      if (x === 0) {
          return (selectCell.innerHTML = '<p class="fas fa-chess-queen"></p>');
      }
      nextStep(x - 1, y);
  }

function nextStep(x, y) {
    try {
        table.rows[x].cells[y].style.backgroundColor = "#ffc40d";
    } catch (e) { }
    return;
}

function clearВoard() {
    for (var i = 0; i < 8; i++) {
        if (i % 2) {
            for (var j = 0; j < 8; j++) {
                if (j % 2) {
                    table.rows[i].cells[j].style.backgroundColor = "#eff4ff";
                } else {
                    table.rows[i].cells[j].style.backgroundColor = "#1d1d1d";
                }
            }
        } else {
            for (var j = 0; j < 8; j++) {
                if (j % 2 === 0) {
                    table.rows[i].cells[j].style.backgroundColor = "#eff4ff";
                } else {
                    table.rows[i].cells[j].style.backgroundColor = "#1d1d1d";
                }
            }
        }
    }
}

clearВoard();
